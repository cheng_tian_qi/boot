package com.boot.boot.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class BootController {

    @RequestMapping(value = "/test")
    @ResponseBody
    public String test(){
        return "Hello world";
    }

}
